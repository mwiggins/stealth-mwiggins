# 2.0.0-alpha01 - 2021/12/13

### Added
- Teddit API: choose between Reddit official API and Teddit API to fetch data (experimental)
- Amoled theme
- Incognito mode by default for keyboard

### Changed
- Improved performances
- Various UI updates

### Removed
- In-app browser: replace with CustomTab
- Max limit for search queries

### Fixed
- Unordered images in galleries
- Comments not saved on configuration change
- Banner not collapsing when navigating to/from Subreddits

# 1.1.3 - 2021/10/25

### Fixed
- Crashes on Android 12+

# 1.1.2 - 2021/08/28

### Added
- Support for redd.it links

### Fixed
- Redgifs videos
- Reddit permalinks opened as external links

# 1.1.1 - 2021/08/19

### Added
- Support for physical keyboards

### Fixed
- NSFW content not showing in search results
- Reddit galleries not loading in saved posts
- Crashes with some user and Subreddit links

# 1.1.0 - 2021/07/17

### Added
- Built-in browser
- Media downloader: save images and videos from posts
- Ability to rename profiles

### Changed
- Improve navigation

### Fixed
- Crashes on some Subreddits
- Wiki links opened as Subreddit links

# 1.0.1 - 2021/05/09

### Fixed
- Fix external links on Android 11+

# 1.0.0 - 2021/05/09

### Added
- Profiles: create different profiles to have distinct subscriptions, history and saved items
- Support for crossposts
- Ability to save posts and comments
- Suggested sort
- Mute button in Media Viewer
- Clickable links in tables
- German translation (thanks to @uDEV2019)
- Spanish translation (thanks to @another-sapiens)

### Changed
- Open drawer more easily on a Subreddit page

# 0.1.1 - 2021/04/06

### Fixed
- Fix Reddit videos not playing when audio is not found
- Fix crash when opening Subreddit from user page
- Fix share feature
- Fix wrong search query being displayed in search page
- Fix for some Gfycat videos not playing

# 0.1.0 - 2021/03/27

### Added
- Subscriptions
- Search
- Sort
- History
- NSFW filter
- Flairs
- Awards
- Light/Dark theme
